
degree_letter(0, 'C').
degree_letter(2, 'D').
degree_letter(4, 'E').
degree_letter(5, 'F').
degree_letter(7, 'G').
degree_letter(9, 'A').
degree_letter(11, 'B').

midi_note_octave(Midi, Note, Octave) :-
  nonvar(Midi),
  Octave is Midi div 12 -1,
  Degree is Midi mod 12,
  degree_letter_accidental(Degree, Letter, Accidental),
  atomic_list_concat([Letter, Accidental], Note).


/* TODO: figure out choice points */
degree_letter_accidental(Degree, Letter, '') :-
  degree_letter(Degree, Letter), !.
degree_letter_accidental(Degree, Letter, '#') :-
  degree_letter(X, Letter),
  succ(X, Degree).
degree_letter_accidental(Degree, Letter, b) :-
  degree_letter(X, Letter),
  succ(Degree, X).

ao(Acc, Oct, Accidental, Octave) :-
  member(Acc, ['#', b]),
  Accidental = Acc,
  number_chars(Octave, Oct), !.
ao(Acc, Oct, '', Octave) :-
  number_chars(Octave, [Acc | Oct]).

/* Spn = scientific pitch notation */
midi_spn(Midi, Spn) :-
  var(Spn),
  midi_note_octave(Midi, Note, Octave),
  atomic_list_concat([Note, Octave], Spn), !.
midi_spn(Midi, Spn) :-
  atom_chars(Spn, Chars),
  [Letter | AccOct] = Chars,
  [Acc | Oct] = AccOct,
  ao(Acc, Oct, Accidental, Octave),
  degree_letter_accidental(Degree, Letter, Accidental),
  Midi is 12*(Octave+1)+Degree.

/* midi note values on left correspond to chromatic scale degrees on right */
/* chromatic scale degrees are [0, 11] */
degrees([], []).
degrees([Midi], [D]) :-
  D is Midi mod 12, !.
degrees([MA | MD], Degrees) :-
  degrees([MA], [DA]),
  degrees(MD, DD),
  sort([DA | DD], Degrees).

/* map chord shapes (lists of degrees as in degrees/2) to their symbols */
/* h/t https://usercontent2.hubstatic.com/1955485_f496.jpg */
degrees_shape([0, 4, 7], '').
degrees_shape([0, 3, 7], 'm').
degrees_shape([0, 3, 6], '\x00b0\').
degrees_shape([0, 3, 6, 9], '\x00b0\7').
degrees_shape([0, 3, 6, 10], '\x2205\').
degrees_shape([0, 4, 8], 'aug').
degrees_shape([0, 7], '5').
degrees_shape([0, 4, 7, 10], '7').
degrees_shape([0, 5, 7], 'sus4').
degrees_shape([0, 2, 7], 'sus2').
degrees_shape([0, 5, 7, 10], '7sus4').
degrees_shape([0, 2, 7, 10], '7sus2').
degrees_shape([0, 2, 4, 7], 'add2').
degrees_shape([0, 2, 4, 7], 'add9').
degrees_shape([0, 4, 5, 7], 'add4').
degrees_shape([0, 4, 7, 9], '6').
degrees_shape([0, 3, 7, 9], 'm6').
degrees_shape([0, 2, 4, 7, 9], '6/9').
degrees_shape([0, 2, 4, 7, 10], '9').
degrees_shape([0, 2, 3, 7, 10], 'm9').
degrees_shape([0, 2, 4, 7, 11], 'maj9').
degrees_shape([0, 2, 4, 5, 7, 10], '11').
degrees_shape([0, 2, 3, 5, 7, 10], 'm11').
degrees_shape([0, 2, 4, 5, 7, 11], 'maj11').
degrees_shape([0, 2, 4, 5, 7, 9, 10], '13').
degrees_shape([0, 2, 3, 5, 7, 9, 10], 'm13').
degrees_shape([0, 2, 4, 5, 7, 9, 11], 'maj13').
degrees_shape([0, 3, 4, 7, 10], '7+9').
degrees_shape([0, 1, 4, 7, 10], '7-9').
degrees_shape([0, 4, 8, 10], '7+5').
degrees_shape([0, 4, 6, 10], '7-5').

/* transpose first arg by number of semitones in second arg */
downshift(_, [], []).
downshift(Offset, [Before], [After]) :-
  After is Before - Offset, !.
downshift(Offset, [B | BB], Degrees) :-
  downshift(Offset, [B], [A]),
  downshift(Offset, BB, AA),
  degrees([A | AA], Degrees).

notes_chord(Notes, C) :-
  between(0, 11, X),
  downshift(X, Notes, D),
  degrees_shape(D, S),
  degree_letter_accidental(X, Letter, Accidental),
  atomic_list_concat([Letter, Accidental, S], C).

notes_chords(Notes, Chords) :-
  setof(C, notes_chord(Notes, C), Chords).
